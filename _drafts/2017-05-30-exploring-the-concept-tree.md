---
title: Exploring the Concept Tree
date: 2017-05-30
---
The concept ontology is one of the most important aspects of the protocol, defining the relationships between different credential communities. It was also one of the hardest to get right.

------------------------------------
A node in the ontology, a concept, is a set of addresses with attached dates and "weights" with one or more other concepts as it's parents.

![Assessments are initiated in concepts and feed into their member pools](/static/img/concept.png)

When an assessment is initiated in a given concept its set of identities will be used to call assessors, and upon a successful assessment an identity is added to the set. The parent relationship comes into play in both steps of this process.

1. When an identity is positively assessed in a given concept they also earn a weight in its parents (and grandparents, etc), decreased by a fixed ratio.
2. When assessors are not available directly from a concept, they are called from the parents.

In this way the populations for child assessors feed into the parents, and can draw from them when needed. This creates the quality that a concept's parents are considered more _general_, i.e that the community the concept describes is a subset of the community its parent describes.

![The ontology can describe interdisciplinary and cross field concepts](/static/img/sampleConceptTree.png)

The ontology has two goals. Firstly it must be able to pull assessors for any arbitrary credential, and secondly it should be able to be crowdsourced, so that concepts can arise as needed.

## Growing the tree
### The seed stage
Anyone can create a new concept by defining its parents. It won't have any members, and so in order for it to be populated users have to get assessed in it. When assessors are called from the parent concepts, they're given an opportunity to determine whether the new concept is valid. They're incentivized to care about this because a child concept feeds back up into the parent.

If it's not valid, the assessors from the parent won't accept the assessment (as it'll be harder-to-impossible for them to come to consensus) and the concept will never have any members.

### Sapling
Because the initial assessors are from a broader community it's possible that the first users assessed may earn a higher weight than they actually deserve. However in this stage the concept will still be dependant on the parents for assessors, just with those who have earned it directly being more likely to be called. This limits the influence initial assessors can have.

More importantly however, they're motivated to increase the validity and reputation of a concept, as they have a stake in it. If an early assessor is malicious and attempts to subvert the assessment process the concept that they're a member of they reduce their own reputation and the value of their credential.

### Moving up the Tree
A concept is established when it has enough members to call them directly for assessments. At this point the parent relationship has fully switched. Instead of the concept drawing from the parent when it needs to it's pushing members up to the parent.

## Creating definitions and coming to consensus
Throughout this entire process, the creation and maturation of a concept, the community around it is coming to consensus on what the concept _is_.

This isn't a one time process, but a continuous one that is expressed through every assessment in a concept. Initially the creator of a concept can attach some data to the concept in order to provide a starting point for the initial assessors. From there however the members of a concept are the ones who need to define what it is.

This can occur through any system really. There could be formal governance around assessment methods, or simply ad-hoc discussion groups. At the end of the day, it's in all the members best interest to take part in _some_ process that helps them come to consensus, as that'll increase all their payouts and the reputation of their concept.

## Evaluating the tree
We think this ontology and the system around it can be applied to any given concept, any idea that's valuable to to assess individuals in, and to come to consensus around. Users of the system are incentivized to care for the tree, to define new concepts and validate them.
