---
title: Why be an assessor
date: 2017-07-03
---
Assessors are the most important community members in fathom. They are the ones who _build_ the community, who define its rules, customs, and ideas. But they aren't a distinct class any member of a community in fathom can chose to be available as an assessor for it. What I'd like to explore here is why they would.

## Cash Money
Okay well not quite cash. But assessors _do_ have a financial incentive. If they assess well (by the standard of the community) they get rewarded with tokens, AHAs. These tokens have real world utility as they're the only thing that can be used to pay for assessments in fathom. As such they have value, and even if the assessor doesn't want to get assessed in the future they could sell them to someone who does.

The converse is true as well. Assessors also have to _stake_ tokens and so won't assess unless they beleive they're able to meet the community standards and come to consensus.

## Stay Sharp
From a pedagogical perspective one of the best ways to solidify one's own understanding and skills is to exercise them. Assessing someone else is a pretty robust application of your skills, as you have to have a simultaenously broad and deep understanding. It's also got an element of competetition to it. The person you're assessing is trying to sneak one past you and you're trying to catch them.

## For God and Concept
Assessors, by definition, have a stake in the concepts and communities they're being called to assess for. They've been through the assessment process themslves, paid their dues to their forebears, and most importantly, have the concept tied to their identity as a credential.

As such, they don't want to damage the communities they're a part of, but instead want to build its reputation and value. The best way to do this is to be an effective assessor. By deciding what makes a valid member they're building the culture, values, and reputation of the entire community, and since they're a part of it, themselves.

------------------------------

## Some nuances
While those latter two are important, especially in the context of the social interactions within fathom, the first point is the most useful. The explicit economic incentives allow us to fine-tune assessors motivations. 

The key example of this is in incentivizing assessors to "double-cross". We can increase payouts when there are dissenting assessors (i.e some who say something waaay off from the majority consensus) in order to decrease trust and increase coordination costs between assessors.

We can also create new tokens when assessors _do_ all come to consensus, essentially allowing the system to grow when it's, under one measure, performing "properly".

Of course this then gets complicated, as the inflation rate should not invalidate the the incentive to snake the others. Fine-tuning these variables, and determining their correct place in the system and process is one of the major challenges we face.

## Culture and Money
What's also especially promising is how the culture of the system, what concepts and communities value and create, can mesh with what the system rewards on a fundamental level.
