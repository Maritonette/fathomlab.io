---
title: Chapter 1 Review
date: 2017-11-26
---

# Deschooling Society - Essay 1: Why we must disestablish school.

The ideas in this essay, the first of seven, can broadly be categorized into
four topics:

1) How institutions aggravate the problems they were designed to address and
       lead to a 'modernization of poverty'.
2) How schools are terrible at providing education.
3) How better solutions to educations could look like.
4) General challenges for education in the 20th century/deschooled society.

For each idea, a collection of arguments is given that back it up:

## Institutions introduce new kinds of poverty!

Illich starts the introduction to this essay collection with his main point:

**"Universal education through schooling is not feasable."**

As a case in point, he points out that the United States, the richest country in the world:  

*"are proving to the world that no country can be rich enough to afford a school
system that meets the demands this same system creates simply by existing."*

A point he later backs up with a studies that calculate the necessary spending
for equal education in grammar and high-school (leaving out higher education
entirely) to be $80 billion dollar per year, which would be more than twice the
amount actually spent that year and roughly the same as the annual cost of the
Vietnam War.

He goes on to argue that schools are just a special case, although a major case,
of the problematic dynamic that unfolds when basic nonmaterial needs are catered
to by an institution: *"nonmaterial needs are transformed into demands for
commodities; when health, education, personal mobility, welfare, or
psychological healing are defined as the result of services or "treatments."*

A new kind of poverty, or a new class of poor, is then defined as those who lack
or fall behing some kind of minimum consumption of that treatment. 

For schools, this polarizing effect of instituions can be extended  
to an international level, as the widely accepted idea of obligatory
schooling puts countries into an international caste system depending on how
many years of schooling they provide to their citizens.

Besides that, Illich identifies three major problems with institutions:

1) Success of the institution is confused with the actual goal. 
2) Improvement will always be directed towards the management of these institutions, instead of
towards those in need. 
3) As a consequence of 1) & 2), individuals increasingly
rely on them, believing that a fulfillment of their needs outside of the
institution is second-class, suspect or outright impossible. There are learning
to be helpless.

Schools play a special role in that dynamic (especially (3)), as they very early
*"school [students] to confuse process and substance."* A pupils is
*"...schooled to confuse teaching with learning, grade advancement with
education, a diploma with competence, and fluency with the ability to say
something new. His imagination is "schooled" to accept service in place of
value."*


In the case of developping countries there is an extra edge, because those will
compare themselves to the north-american standards of education (e.g. 12 year
attendance),already unattainable for the US, are economically absolutely absurd
for them. Using Illich's words those countries will "*think rich and live
poor"*. A mindset that makes them accept increased public spending on education
of very few and discourages them from taking education in their own hands.

Illich definitely is in favor of universal education, yet warns that *"to equate
it with obligatory schooling is to confuse salvation with the church"*.

## Schools are terrible at providing education...

Arguments for this point can be divided into negative impacts schools have on
the students in them and on society as a whole. Let's look at the students first:

### ...to students

Illich's major critique, already touched upon above, is that schools confuse
learning with witnessing teaching: The qualities and compentences with the
process (instruction) by which they are supposedly adquired.

This neglects that learning can happen in a variety of forms. In many of them,
learning happens casually or as a byproduct of some other leisurly activity,
such as when a new language is learned by travelling or by meeting a new
partner. Secondly, skill teaching programs are unneccessarily tied amongst each
other (advancement in history depends on progress in math).

Being in a school, a pupil experiences how social role-assignment and learning
are melted into one: Depending on how well his learning matches a predefined
curriculum he is assigned a role (3rd grade, 4th-grade but also social roles
within a class). Illich observes that this is not a new phenomenon:

*Curriculum has always been used to assign social rank. At times it could be prenatal:
karma ascribes you to a caste and lineage to the aristocracy. Curriculum could take the
form of a ritual, of sequential sacred ordinations, or it could consist of a succession of
feats in war or hunting,...*

The irony here is that *"Universal schooling was meant to detach role assignment
from personal life history: it was meant to give everybody an equal chance to
any office.[...] However, instead of equalizing chances, the school system has
monopolized their distribution."*

Another argument, not from this but from the second essay, states that a liberal
society can not be founded upon the modern school system, because the relation between the
teacher and the pupil is so authorative:

*"Children are protected by neither the First nor the Fifth Amendment when they stand
before that secular priest, the teacher. The child must confront a man who wears an
invisible triple crown, like the papal tiara, the symbol of triple authority combined in one
person. For the child, the teacher pontificates as pastor, prophet, and priest-he is at once
guide, teacher, and administrator of a sacred ritual. He combines the claims of medieval
popes in a society constituted under the guarantee that these claims shall never be
exercised together by one established and obligatory institution--church or state.[...]
Under the authoritative eye of the teacher, several orders of value collapse into one. The
distinctions between morality, legal. ity [sic!], and personal worth are blurred and eventually
eliminated. Each transgression is made to be felt as a multiple offense. The offender is
expected to feel that he has broken a rule, that he has behaved immorally, and that he has
let himself down."* 

Such a role can only be justified under the paradigm of instruction as the only
legitimate way to learning.

### ... to our society

The main effect on society is that the education-by-schooling-paradigm has is
that it's holding back all other institutions and individuals from taking on
educational tasks.

From the perspective of institutions, Illich points out how much many of them
depend on schools for the *"habits and knowledge they presuppose"*, yet stay
inactive instead of becoming themselves means of education.


On the level of individuals, the profession of the teacher (or rather the
licensing) is preventing non-professionals from acting as teachers and
imposing a too rigid framework in which teaching can occur. School-teachers are
limited by what they can teach in the context of schools and neglects a vast
variety of alternative or maybe unorthodox ways that skills can be taught.
Therefore Illich points to studies that showed that a bunch of teenagers, if
incentivized and shortly trained, are much more effective than grown-ups at
introducing their peers to a range of scientific subjects than certified
teachers. 

Another example mentions a brazilian teacher Paulo Freire who shows
that grown-ups will start learning to read within 40 hours if the first words
they learn are charged with political meaning relevant to them. 

These and any other ideas which will partially layed out in the next section,
are held back by licensed teaching, which can be seen a form of undue market
manipulation. Undue because everybody who can exercise a skill is in theory
qualified to instruct it. 

## Proposed solutions

In this first essay Illich outlines three ideas: an edu-credit that could be
redeemed throughout an individuals life, a new approach to incidental learning
based on match-making and the most radical one: A bill of rights forbidding the
state to meddle in an individual's establishment of education. Let's look at the
less drastic ones first:

### edu-credit

Illich envision an "educational credit" given to each citizen at birth, which
would entitle the holder to 12 years* worth of instruction that could be
redeemed for at different skill-teaching instiutions or facilities throughout
the entire life. Such a credit would especially benefit the poor, who leave
school earlier than others. Later in life they could than come back and learn
whatever skill they deem appropriate for their respective situaiton.

Realizing this vision would require a variety of new kinds of learning
opportunities - a challenge that with the help of unlicensed teaching could be
met via a variety of solutions, one of which described in the example in the
next section.

### Skill centers and a new approach to incidental learning 

Opening up the 'teaching-market' to unlicensed teachers creates many
opportunities for learning to occur. One idea, that dovetails with the
edu-credit from the section above are skill-centers that blend the teaching,
learning and the service to a customer, who would serve as ultimate judge on the
quality of work.

As an example how these skill-centers could be very effective and draw from
previously untapped resources, Illich recounts how a friend of his, Gerry
Morris, trained 300+ members of the New York Archdiocese in spanish so that they
could communicate with the puerto-rican community. To find his teachers, he had
announced in a spanish radio station that he was looking for native speakers
from Harlem. The next day, there were approx. 200 teenagers in front of his
office, of which he trained about 50 how to use a spanish manual designed for
linguist graduates. After a week of training how to use the manual, he send them
of to be teachers themselves. Within 6 months, the task was accomplished.

Illich also envisions less formal, incidental approaches to learning: In few
sentences Illich proposes a match making system (based on letters!), where
people with similar interests can find each other 'within only a few days'. He
lines out how many places not traditionally thought of as educational premises
could serve for educational encounters, such as bars, trains and parks.

The major advantage of these incidental, informal encounters between strangers
is that their circumstances are in many ways much more favorable for true
learning to occur.

*"Education in the exploratory and creative use of skills, [...] relies on the
relationship between partners who already have some of the keys which give
access to memories stored in and by the community. [...] It relies on the
surprise of the unexpected question which opens new doors for the inquirer and
his partner."*

In other words: These "*meeting[s] among people who share an issue which for
them, at the moment, is socially, intellectually, and emotionally important*"
are less predictable in their result, but also holds *"unlimited opportunity"*.

Noteworthy, this shift to learning with a partner does not remove the teacher
from the equation, it merely assigns it a new task: To support the matching
process by "*help[ing] the student to formulate his own puzzlement since only a
clear statement will give him the power to find his match, moved like him, at
the moment, to explore the same issue in the same context."*

A much more fundamental and radical idea that would also motivate people much
more to educate themselves via these ways is presented in the next section.

### The bill-of-rights of education

Illich is no fan of the state's role in education, which he likens to that the
church about 250 years ago. Like the church back then, schools permit the state
to *"ascertain the universal [...] deficiencies of its citizens and establish
one specialized agency to treat them."*

Back then, the US played a big role in disestablishing the monopoly of the
curch, when it declared that in its first amendment that the state shall make no
law with respect to the establishment of religion.

Illich next step goes consequentially along the same lines, as he proposes:

*"The state shall make no law with respect to the establishment of education."*

*"To make this disestablishment effective, we need a law forbidding
discrimination in hiring, voting, or admission to centers of learning based on
previous attendance at some curriculum. This guarantee would not exclude
performance tests of competence for a function or role, but would remove the
present absurd discrimination in favor of the person who learns a given skill
with the largest expenditure of public funds or what is equally likely has been
able to obtain a diploma which has no relation to any useful skill or job. Only
by protecting the citizen from being disqualified by anything in his career in
school can a constitutional disestablishment of school become psychologically
effective."*

Further on, he elaborates: 

*"To detach competence from curriculum, inquiries into a man's learning history
must be made taboo, like inquiries into his political affiliation, church
attendance, lineage, sex habits, or racial background. Laws forbidding
discrimination on the basis of prior schooling must be enacted. Laws, of course,
cannot stop prejudice against the unschooled-nor are they meant to force anyone
to intermarry with an autodidact but they can discourage unjustified
discrimination."*

While realizing such an idea would pose huge challenges, and its feasability
might seem questionable to today's reader, it certainly would change people's
attitudes towards alternative, informal and incidental education opportunities
such as they are outlined above. Also, it would shift the responsibility,
initiative and accountability for one's education from the state back to the
learner. In other words:

*"Education for all also means education by all."*

## General challenges for education in a deschooled society

In traditional societies and medieval towns, all learning was lifelong,
incidental. The essential structures of a villagers life - language,
architecture, work, religion and family were all intertwined and mutally
reinforced each other, such that *"growing into one implied growing into the
other.[...] If an apprentice never became a master or a scholar, he still
contributed to making shoes or to making church services solemn."*

Learning did not compete for time with work. Yet life in contempary societies is
much less intertwined *"the modern man must learn to find meaning in many
structures to which he is only marginally related."* 

To that end, Illich proposes that we must design educational opportunities into
the structures we build and that the educational quality of all institutions
must increase. He warns that this is an ambivlanet promise: On the one hand,
this could lead to a life of constant instruction and manipulation. On the other
hand, he hopes men will *"shield themselves less behind certificates acquired
in school and thus gain in courage to 'talk back' and thereby control and
instruct the institutions in which they participate."*

Thus, next to the challenges outlined in the earlier sections, e.g. returning
initiative to the learner and creating more appreciation of incidental learning,
the main challenges for a deschooled society are to provide more opportunities
for educational give-and-take and, consequentially, to lear how to measure the
educational quality of institutions and activities in general.


DISCLAIMER: This document does not represent the views or opinions of the
author, but rather the auhtors understanding of Iwan Illich's views as he
explains them in his first essay. If you feel I misunderstood, please reach out
to us.

