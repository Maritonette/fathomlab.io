---
title: 'Meetup 1: Talking to Children about Blockchains'
date: 2018-03-28
---

This is to organize our first meetup! The process is likely to be rough, so
let's soldier through.

## Details:

You can find all the details in an easily calender-able form [here](https://calendar.google.com/event?action=TEMPLATE&tmeid=Nmo2OWh0OXVkbTZoaGo4YXVzM202NmhrZXUgY29uc2Vuc3lzLm5ldF80c2pvaW01Mm8zNnU3b21ldXBqdTgxZGdmc0Bn&tmsrc=consensys.net_4sjoim52o36u7omeupju81dgfs%40group.calendar.google.com)

- **When:** 8PM UTC, April 4th
- **Where:** [This Jitsi video chat](https://meet.jit.si/WorriedFruitShootSelfishly)
- **What:** Talking to children about blockchains!

## Topic
The topic is fairly open ended but here are some interesting questions to
explore. 

- How do ideas of trust, security, and consensus relate to a young humans
  experience? How does this change as they grow/learn?
- Will children interact with the systems we're building, _in their current
  forms?_ Things like key-management and smart-contracts. If not, how will they
  change? 
- What are the _abstract_ ideas of this space and field that are most broadly
  useful in learning? How have they changed how you view the world?
- Are we as a community open and engaging for young humans?

### Material
There's nothing in particular this week as we're really just starting up. That
being said
[Mindstorms](http://worrydream.com/refs/Papert%20-%20Mindstorms%201st%20ed.pdf)
(as I've mentioned before) provides an incredible perspective on children,
computers and learning, that I think extends pretty well into our discussion
topic.

For (very) rough initial exploration of that, you can check out the [talk I
gave](https://www.youtube.com/watch?v=1Bk4jB4p4rM&t) at EthCC. 

## Outcomes
This is a short retrospective and summary of the conversation. I meant for it to
be recorded, but blanked in the excitement of conversation.

We had a small but awesome collective of around 7 people.

### Lot's of questions, few answers
The conversation was a little bit messy, acknowldged a couple times as so. One
of the key goals moving forward will be to have smaller scopes for these
meetings.

#### Explaining the tech vs explaining the impact
This question is one that's come up a bunch in multiple conversations I have.
Roughly speaking the benefit of explaining the impact is that an individual can
understand how it might relate to their life, and the benefit of explaining the
technology is that an individual may be able to _apply it_ to their life, in
potentially novel ways. 

Balance most likely. 

#### What do we call this field?
Personally I've been referring to it as distributed or social computing, but it
was pointed out (rightfully) that that isn't the most evocative of phrases. One
suggestion was using the language of social organization and cooperatives. Or
just sticking to "blockchains"

#### What are the pre-requsites? 
A focus of our conversation was around the pre-requisites. We had people of
varying backgrounds present, who came to learn about blockhains in different
ways, and so started from a variety of different perspectives. 

More than anything I think this meeting reaffirmed the need for a broader
conversation around the topic of learning in this space. There was so much that
we could spent hours individually diving into, and creating a space for this
dicussion will be very valuable.

### Moving forward:

- Set up a repository for information, materials, and tools. Create a reading-list.
- Set up a mailing list of some kind for the next meeting.
- Decide the topic, find a speaker, set the date

