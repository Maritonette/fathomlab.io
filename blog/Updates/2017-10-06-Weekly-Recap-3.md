---
title: Weekly Recap 3
date: 2017-10-06
---

Tad late this week. 

## Chat rooms

The major feature added to chatrooms is the ability for it to store contracts in the metadata and enable their calling through the use of /commands. You can check out how we did it in [this ](https://gitlab.com/fathom/chat/commit/3228669f11ba6e95501ebf7fa8aaf0a6090e7509)commit. It's a tad janky at the moment, without easy error checking. 

An important feature to add to this will be a way to discover what commands are available and what they do. There are several options for architectures, which will be discussed on [this](https://gitlab.com/fathom/chat/issues/23) issue.

We (specifically Jared) lost a bunch of time time trying to set up SSL on AWS EB for the darq.chat domain, so instead it's been taking down. Hopefully we'll get it up an running again, hosted somewhere else most likely.

## Rebooting Web of Trust

Jared spent the majority of this week at the Rebooting Web of Trust workshop in Cambridge. 

It was a massively informative experience, especially in figuring out DIDs and how they can relate to fathom. 

One big takeaway, based on something Christopher Allen said regarding business opportunities in the coming paradigm, was that a lot of the workshops work has the assumption that credential issuance is fundamentally a centralized process. 

I don't really think that makes sense, and it operates on a fairly narrow definition of what a credential is. We are biased since what we're building with fathom is *literally* decentralized credential issuance. I hope to collect my thoughts more on this and publish something soon.

## Whitepaper

Julius put together the first draft, but there's still a ton of polishing to be done. Most of the content is in place though, and so things are progressing smoothly. 

Soon we should be able to transition to a more open development cycle for it, hosting it either on the repo for this website or in it's own place. 

## The website
We still haven't moved forward on the redesigning of the website. However there are some posts sitting on the backburner that we should be bringing out soon. 

We also kinda dropped the ball on putting together the book-club, but we should nail down the structure for it and get something out there by mid this coming week.
