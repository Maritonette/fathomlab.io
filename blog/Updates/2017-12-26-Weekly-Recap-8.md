---
title: "Weekly Recap 8: Happy Holidays!"
date: 2017-11-26
---
These weekly recaps are becoming more monthly. A middle ground will probably be
the most useful, biweekly or tri-weekly. As we figure out our development
cycle/sprints/etc this will become clearer. 

Chats
-----
In the last month we've been dealing with mostly high level work on the
chatrooms project (side note: if anyone comes up with a name let us no,
internally we've been playing with "speakeasy")

Our main focus has been getting events to work correctly, both listening for new
ones and parsing past. You can take a look at some of this work in this 
[merge request](https://gitlab.com/fathom/chat/merge_requests/23).

Moving forward we're figuring out how to make metadata more generalized as to
allow for "packages" of functionality to be loaded and easily used. 

We're also working on making the text input/command system cleaner, as well as
getting notifications set up so that this stuff is somewhat usable.

Tokens
------
A really fun part of our work has been around getting the token dynamics sorted
out. We decided early on that we weren't super interested in the idea of
a token drop. This is for a couple different reasons (which we will
explore in a [blog post](https://gitlab.com/fathom/fathom.gitlab.io/issues/35)
soon)

So, we want to distribute tokens in a rolling fashion to the participants of the
network. In order to do this we need a definition or measure of
participants in the network, which we pretty get from participants in
assessments. 

Essentially, in a given time period, participants in assessments enter a lottery
for newly minted tokens. Participating in more assessments = more entries into
the lottery. The similarities to PoW mining are pretty apparent.

The nice part of this is that it's very easy to implement, and to then tweak the
variables, such as issuance rate and amount.

The first part of this work is [here](https://gitlab.com/fathom/assess/merge_requests/94)

Push and Pull
----
Our current strategy has been to host content on this site and direct people to
it when relevant. While this is handy, having to constantly check the website is
not the best experience for anyone who wants to keep up with fathom. 

With that in mind we're setting up a
[newsletter](https://tinyletter.com/fathomNetwork). Updates will be more
occasional than the blog, and will also include external content, and things we
just generally find useful. You can click that earlier link to subscribe. 

Book Club
-------- 
For our next book we're going to be reading either [Pedagogy
of the Oppressed](https://en.wikipedia.org/wiki/Pedagogy_of_the_Oppressed) by
Paulo Freire or [Mindstorms](https://en.wikipedia.org/wiki/Mindstorms_(book)) by
Seymour Papert. If you have anything else, you think would be fitting, please
send it our way.

That's all for this ~~weekly~~ monthly update. 
