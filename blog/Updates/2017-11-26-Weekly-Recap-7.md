---
title: "Weekly Recap 7: Reacting and Reading"
date: 2017-11-26
---

A busy week! We've had the whole fathom team down in Dubai working on hard on a whole host of things. 

## Reactin!
The chat app was built on a pretty shaky foundation put together over just a couple of days (somewhat as a bet). It was largely based on the excellent [hack.chat](https://github.com/AndrewBelt/hack.chat) from Andrew Belt, with all the Ethereum stuff hacked on top.

Side note: That code base was massively helpful for groking web-sockets in a self-contained way. 

Anyways, we've spent the last week redoing the front-end in React and polishing up the backend to be a little more stable. Hopefully going to be deploying the new UI soonish (next couple of days)

## Reading 
So this has been on the backburner for ages but we finally got around to collecting to our thoughts on Illich and Deschooling society. We're gonna be publishing that tommorow. 

In addition to our thoughts we're also planning to use Genius to work through some annotations of the work + a summary Julius wrote. It'll be interesting to see if that's a valuable tool for these kinds of discussions.

## Token things!
We're working on a token issuance model. Currently we're focused just on the issuance mechanism, deciding who gets the tokens, rather than the issuance rate, but we'll get to that soon.

Currently we're thinking a weighted random distribution among assessors with weight being proportional to the amount of tokens staked on assessments.

This discussion was prompted largely by our work on the bootstrapping process.

## Bootstrapping Program
We're slowly working out the details. Essentially it's a trial run of the fathom ecosystem among particularly motivated and specifically chosen peers. It will transition directly into the open ecosystem as the network grows. 

To facillitate this there will be no meanigful difference for participants during the duration of the program and after it. 

However, we are planning to offer an incentive to particpants to be distributed during the duration, some mixture of ETH and FathomTokens (ranging from 0 - 100%). This is what prompted our discussions are token issuance mechanism.

In the next two weeks we plan to release an overview of the program and what we hope to accomplish with it. 

## A tiny bit of bash
Our blog page has changed so that we can nest folders and have it displayed nicely. Currently we have a very janky bash script + pandoc doing this all. 

That's all, thanks.
