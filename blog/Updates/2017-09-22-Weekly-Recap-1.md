---
title: Weekly Recap 1
date: 2017-09-22
---
## Catching Up

Since we last posted an update we moved through a pretty big refactor of the code-base. This involved dealing with a ton of different issues, restructuring smart-contracts, and building out a test-suite. 

## CodeBase

We removed the `User.sol` contract and the user abstraction, letting proxy's be handled elsewhere or later. 

Concept's can now have more complicated relationships to one another in the form of connection strengths, which specifu how much a member's assessment in a child-concept should be considered relevant in the parent concept.

We implemented expiration dates in assessments and concepts that allow weights to only be valid for a set amount of time. 

## Gas

A simple gas-costs analysis revealed the approximate costs of a fathom assessment. Assuming 1 ether = $217 and a minimum safe low cost of 1 GWei as gas price we found the following costs associated to the main activities on the fathom net:

        * creating a concept: $0.65
        * creating an assessment: $0.49
        * committing a score: $0.017
        * revealing the last score (which will trigger the computation of the result): $0.15

We also built out a mechanism to seed the initial network with a [Distributor contract](https://gitlab.com/fathom/assess/blob/master/contracts/Distributor.sol). 

A lot of this was driven by the onboarding of a new full-time developer, Julius Faber who joined in June.

## Front-end

We've begun work on building out the front-end application to go along with the protocol. This has consisted of exploring different interaction mechanisms, some simple wireframing, scaffolding our stack/architecture.

### A new app
Along side this we've also started on putting together a smart-contract controlled server-based [chat room](https://gitlab.com/fathom/chat) as a separate application. This is the first iteration, and hopefully will grow from here. It'll serve different purposes within the fathom ecosystem, but is primarily a platform to define social environments based on credentials. You can play around with it on [https://darq.chat](https://darq.chat/) and the rinkeby testnet.

Side note, but developing this has been super fun

## Website
We started laying out a whitepaper. Though we're a fan of the lightpaper and more casual content we have right now, as fathom grew in complexity we realized that we need a technical specification to point interested parties to. As of right now, the whitepaper only exists in the form of bullet-points on a separate [branch](https://gitlab.com/fathom/fathom.gitlab.io/tree/whitepaper/whitepaper) in our repository, but we expect to have it written out and published within the next week.


With more material being ready and a whitepaper in the works, we are redesigning our website to cater to a wider audience as well as laying out what you can do to become involved in the development or when fathom is ready to be tested and launched.

This'll give us a strong foundation to create thought and community around what we're building.

# Moving Forward
We are going to be doing this once a week, usually on a friday. They all probably won't be as intense as this one, as this had to deal with a lot more recap. Hopefully this will help keep us accountable and create a little more public awareness. 
