---
title: First Assessment!
date: 2018-04-28
---

Being in the same place for about 3 weeks, we hit a nice stride. 

## App-Prototypes: From many to one

We experimented a bit with truffle's new drizzle-framework, but in the end
decided to go for a classic react-redux app, handling all transaction and events
ourselves.

After having experimented with the different views of our assessment app, we
merged all of them together and built a first working prototype of our app. It
is still very janky and is missing some basic parts such as proper monitoring of
transaction status and event-listening, but it displays information on
past, present and future assessments as well as methods to commit and reveal
scores.

You can check it out on your local testnet, by following 
[these instructions](https://gitlab.com/fathom/assess/tree/master/app).

## dev, dev, dev

We now have a development-version of the fathom-network on rinkeby. Other than the
original protocol, it allows us to run assessments with only two assessors and cut
out the challenge period after the commit period.

The central piece, the _ConceptRegistry-contract_, is at
_0x4e1d76f88e31965be788d82c6154455dc7f02d84_ and our _FathomToken_-contract is at
_0x06caae3633578cff02e8e1c88b5421903777e4bb_.

Next, we are working on making our development process more accessible, e.g. by
creating scripts that modify truffle's build folder to be aware of that deployed
version of the network. That way it will become really easy for us to develop
different versions of the frontend while interacting with the same network-instance.

## Bridger
Bridger is an app we're developing to create and verify claims -- check out the 
repo [here](https://gitlab.com/fathom/bridger). It provides a way for users 
to link their ethereum identities to other disparate accounts they own 
(e.g., their other web 2.0 identities) by creating verifiable claims around 
those identities. The goal is for individuals to bridge their various identities 
in ways they choose, enabling richer interactions around blockchain and distributed data.  

Bridger began as a command line interface, and is now evolving into three apps 
-- the command line, a web app, and a library we'll be publishing to npm. 
We've been building out the web app in react and architecting it to manage 
various claim types and numerous claim instances. We've started implementing 
typescript and json-schema for stronger data validation. Next up, we're working 
to unify the components into a single page, enable the chaining of claims, and improve
the structures that organize the claim handlers. Stay tuned!


## Roadmap in Milestones:

To be more aware of the bigger picture, we formalized our roadmap using GitLab's
_Milestone_-feature. It neatly combines issues into 
[project-](https://gitlab.com/fathom/assess/milestones) and 
[group-level](https://gitlab.com/groups/fathom/-/milestones)
milestones and thus automatically reflects progress and changes to our plans.

For example, check out [this](https://gitlab.com/groups/fathom/-/milestones/4)
milestone for our masterplan to launch the playground: It entails (in more or
less detail) all the steps we need to complete until we will open the
playground.

Among the active milestones right now, the one which is most close to
completion right now is the ["Run an assessment!"](https://gitlab.com/groups/fathom/-/milestones/2),
which combines issues from bridger and the frontend-app, such as a new bridger 
[claim-type](https://gitlab.com/fathom/bridger/issues/14) to proof that one has 
earned a fathom-credential. 


## The Playground

The playground specification is now in its 3rd iteration, and we've moved its
[repo to github](https://github.com/jaredpereira/playground). 
We're really excited with how it's taking shape! We hope the description conveys 
both clearly yet concisely what the playground will be all about and how to participate.

