---
title: Recap and Rough Roadmap
date: 2017-06-02
---
We are aiming to have an alpha version of a fathom client by devcon 3 on November 1, 2017.

This will look like (as I envision it right now, all is extremely subject to change) a mobile-friendly web application centered around chat rooms for assessments and community organization.

Individuals will be able to:

- get assessed
- assess others
- transfer the native token 
- create new concepts

Nice to have would be the ability create groups/communities around credentials. However this can be approximated with an API that let's other services leverage credentials, which is fairly straightforward to do with the current MetaMask/Mist paradigm.

## Recap and Moving Forward
The protocol itself is in a good place. There are still some details, such as how we deal with [inflation and other economic constants](https://gitlab.com/fathom/assess/issues/3), that need to be worked, but these are not blockers and can be mocked while development progresses on other fronts.

### Code
The [Solidity code](https://gitlab.com/fathom/assess/tree/master/contracts), while it exists and functions, is extremely rough. Testing still needs to be done, and the entire thing needs to be cut down to size. However, while this may require some _rough_ rearchitecturing, the basic structure shouldn't change too much.

The code was written as an aid to developing the protocol itself. It allowed us to more easily uncover assumptions and issues as we tried to build something practical. Now that we have a clear idea of how the entire system functions we're in a better place to make some proper, robust smart-contracts.

### Writing
Explaining how those contracts will work, and the entire system functions is another question. 

We have a minimal amount of writing published on the depths of the system and its impact. What we do have is scattered and outdated. This site is the first step towards remedying that, as we'll be rewriting what we have (such as [this](https://blog.otlw.co/fixing-a-broken-metaphor-part-1-80f296fd214d) and [this](https://blog.otlw.co/the-kids-need-ethereum-39085f64c8ac)) and creating a space for new content to easily be added and indexed. 

There's a large configuration space for communities built ontop of and interfacing with these credentials, and mapping it out is pretty important. I'm honestly pretty excited to explore it all.

Also we need to work on lower level documentation. The current system explainer on this site is fairly robust, but needs to be made more explicit and concise. Also as we build more this will become more important.

This writing all needs to be done in the public, and be published consistently.

### Community
Ideally this work won't be done in isolation. While we don't plan to wait on anyone but us to get shit done, we want to set the stage for anyone who wants to to be able to contribute. 

Supporting this community of users and contributors will be a major point of focus moving forward. There _was_ a gitter but it was extremely under-utilized (I think I had maybe 2 interactions with outside individuals there). Currently a lot of the development is facilitated by a private slack, but we'll hopefully be moving away from that to something more open.

## Shifting Grounds
As we build and things happen I'm sure many of these aims will change. However the core focus of open, consistent, and meaningful development of every aspect of the project will not. 
