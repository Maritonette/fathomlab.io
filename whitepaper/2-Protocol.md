The Fathom Protocol {#sec:protocol}
===================

This page has four parts: First, an overview is given of the different elements
that make up the fathom network. Second, the process of creating certificates
and being assessed in them is presented step-by-step. The third part lays out
the incentive structure that makes it economically disadvantageous to deviate
from the protocol. The last part presents potential attack vectors and how they
are mitigated.

Architecture Overview {#sec:architecture}
---------------------

This section will introduce the three main components of a fathom-assessment:
First and second, a *concept*, representing an assessable quality and the
*concept-tree*, which relates all concepts to each other. Third, *assessments*
which draw individuals from the concepts to be assessors and, upon a positive
assessment, add new individuals to them.

### Concept {#sec:concept}

*Concept* is an umbrella term to capture any kind of skill, quality,
piece of knowledge or fact that can be established about an individual.
Therefore, each concept $C$ has the following properties:

-   **Parent Concepts**: This is the set of concepts $P$ that $C$ is a subset of
    . For example, the concept 'Math' could be a parent of 'Linear Algebra'. If
    there is no suitable parent, a concept can be located beneath the
    'mew'-concept - a specially designated concept with no skills associated to
    it and with no parents.

-   **Connection-strength(s)**: For each parent $p$ $\in P$, a concept $C$ denotes a
    connection strength $c_p$ from 0 to 1, specifying the degree of
    similarity or difference. Upon a positive assessment, $c_p$ is used
    to determine how much someone who masters the child-concept should
    be considered competent in the parent-concept (see the 
    [incentive section](#sec:acceptAss){reference-type="ref"
    reference="sec:acceptAss"} for a more detailed accounting of how the
    $c$-value is determined).

-   **Expiration time**: Concepts can specify expiration times $e_c$ to reflect
    that some skills become outdated, need to be maintained, or change over
    time. An example would be concepts related to taxation-laws, which are
    changed on a relatively frequent basis and where false or outdated
    information can lead to significant losses. Members who have been assessed
    in a concept longer ago than specified by the expiration time do not lose
    their certificates, but can no longer take part in the process of assessing
    others.

-   **Members**: a set of individuals who have passed an assssment in the
    concept in question or in one of its children
    -  **Component Weights**: For each member the concept stores a set of
    component weights, a positive integer and date, corresponding to
    assessments in the concept or it's children. The sum of valid component
    weights, those which do not have a date longer ago than the concept's
    expiration time $e_c$, is used to probabilistically call a member to act as
    assessors in assessments.[^1]

### The Concept-Tree {#sec:concept-tree}

As all concepts have at least one parent, the entirety of all concepts forms a
tree. The 'mew'-concept is at the root and as one moves down concepts become
more specific. The weighted parent-child connection between concepts is also
reflected in the the weights of their members: Individuals who have been
assessed in a child concept are also added as members of the parent concept(s),
with their weight(s) reduced by the connection strength. The set of individuals
in a child concept can therefore be seen as a specific subcommunity of the
parent-community. Individuals are therefore members of many more concepts than
those they have been assessed in, although those will be the ones where their
weight will be highest.

This nesting of knowledge-communities is valuable when sampling members to
create a pool of potential assessors. If a concept has not enough members for
the pool they can be drawn from the parent concepts. The members of a parent
concept will have related experience(as it has been populated by the concept and
siblings), and hence have a higher probability of being able to assess the
concept in question.

### The Assessment Game {#sec:assgame}

An assessment is the process by which a jury of qualified individuals
(assessors) decides whether or not some candidate (assessee) fulfills
the necessary conditions to become a member of a concept. When
initiating an assessment in a concept, the assesse decides how many
assessors they want and how much they are willing to pay to
each one of them. That offer is forwarded to potential assessors (see
[setup](#sec:setup){reference-type="ref" reference="sec:setup"} for
drawing specifics) who must stake the offered amount in order to accept.
Thus, a market forms around assessments, allowing the system to scale
from easy to assess, and hence cheap, concepts, to more involved,
complicated, and hence expensive ones.

Upon completion of the assessment, assessors are paid the price offered
by the assessee and a proportion of their stake if they come to
consensus around the applicants skill. The proportion of the stake being
paid back is proportional to the assessor's proximity to the average
score of the biggest cluster of scores and will also be added a portion
of the stake of dissenting assessors, should there be any (see
[payout](#sec:payout){reference-type="ref" reference="sec:payout"} for
details).

Also, the mechanism by which assessors log in their scores is designed
such that colluding assessors can double cross each other, thereby
creating a coordination problem in an adverserial environment, where the
only point of coordination (schelling point) left is a truthful
assessment. In case the majority vote of the assessors is positive, the
assessed candidate will get i) a score in the assessed concept, similar
to a grade in university or school, ii) a weight in the concept and iii)
a weight in all parent-concepts, proportionally reduced by their
respective connections strenghts.

Assessment Process {#sec:assprocess}
------------------

A fathom assessment goes through five phases: A setup phase, where the
assessors are called from the concept tree, the assessment, where the
assessors determine the assessee's skill, commit- and reveal-phases,
where the assessors log in their score and, at last, the calculation of
the result.

For each phase this section is gonna depict the choices of the involved
participants, their interactions and what happens if they deviate from
the protocol.

### Setup {#sec:setup}

#### Creating the assessment: {#sec:createAss}

Wanting to be certified in a concept $C$, the assessee needs to specify
the following parameters:

1.  A timeperiod during which they would like the assessment to start and
    end (latest start- and end-time).

2.  The number of assessors $N_a$ to be assessed by. While there is a
    minimum number of five assessors to guarantee a fair voting, the
    assessee might want to be assessed by a bigger number in order to
    receive a higher weight and higher chances to become assessors
    themselves (given that they end the assessment with a passing
    score).

3.  The price $cost_a$ that each assessor will be paid.

#### Calling assessors from the concept-tree: {#sec:sampling}

A pool of potential assessors is created by probabilistically drawing
members from the concept and its parents until a pool size $N_p$ of 200
times desired number of assessors is reached or the mew-concept is
reached. The selection of potential assessors happens according to a
tournament-selection of size 2, starting at the assessed concept:

-   Two members are picked at random and their weights are compared.

-   Only if the weight of the first member is bigger than the second
    one, the first is added to the pool, given that they are not already in
    there.

Thus it is assured that each member has a chance of being called as
assessor (the chance of being drawn first), whilst giving a higher
chance to members with higher weights).

As it is crucial that participants can not foresee who will be called as
assessor, no more than half of the members of each concept can be called
as assessors. Therefore, this process is repeated at most $Y$ times for
each concept at hand, with $Y$ being the remaining number of required
assessors given that this number is smaller than half the number of
members in the concept $m_c$:

$$ Y = \min (200*N_a, \frac{m_c}{2}) $$

After $Y$ attempts, (on average resulting in $Y/2$ members being added)
the remaining $r = 200*N_a - \text{poolsize}$ are drawn from the
concept's parent. In the case that there is not only one parent but $p$,
the process is repeated trying to draw $r/p$ from each concept (again
limited to a maximum of at most half the members of the respective
parents concepts, as described by the above equation.

#### Assessors confirm by staking: {#sec:confirm}

Each assessor that is being called, can decide to participate in it by staking
the offered price. Once the desired number of assessors has confirmed, the
assessment moves to the next stage. Assessors from the pool self-select whether
they think would be competent judges on concept in question. If so, they signal
their intent to participate by staking the offered price. More considerations
why assessors would or wouldn't want to confirm are elaborated
in [incentivization](#sec:acceptAss){reference-type="ref"
reference="sec:acceptAss"}.\ If not enough assessors can be found before the
desired start-time of the assessment, the Assessment is cancelled and everybody
who deposited collateral is refunded.

### Assessment of the candidate {#sec:assass}

In a fathom-assessment there is no notion or form what constitutes a
test and the form or procedure of how candidates are evaluated is left
to each individual assessor. Ultimately, assessors express their verdict
of assessee's skill as a number on a scale (e.g. between 0 and 100) -
with everything above half being considered a passing score.

Yet, what exactly defines a failing, passing or barely passing
assessment can be different for each concept as well and should be
agreed upon by the community (see
[frontend](#sec:frontend){reference-type="ref"
reference="sec:frontend"}). Moreover, the assessment could also be the
place to put up some sybil protection mechanism in the form of extra
requirements that make it hard to repeat an assessment (see
[sybil-attack](#sec:sybil){reference-type="ref" reference="sec:sybil"} for more
details on how this could work).

### Committing a Score {#sec:commit}

Sending in a score follows the commit-reveal procedure common in
blockchain applications. Assessors signal that they have decided on a
score by concatenating it with a secret element, also refered to as
'salt' and submitting its hashed value (hash=sha3(score+salt)).

If any assessors fails to commit a score before the assessment ends
their stake is being burned. If, as a consequence, less assessors than
would be required for the minimum size of a viable assessment have
committed, the assessment is cancelled and everyone is being refunded.
Otherwise, the assessment progresses to the next stage.

### Steal and Reveal {#sec:stealReveal}

To end the assessment, the assessors reveal their verdict by submitting
their score and salt separately. Any assessor (or external person) who
knows about another assessor's score and salt, can do so as well,
thereby stealing half of the assessor's stake, burning the rest and
eliminating him/her from the assessment game. This prevents the
assessors from credibly garuanteeing each other their committment to
logging in a specific score, thus making it harder to collude.

While stealing is possible at all times after an assessor has committed
(even if others have not yet), revealing will only be possible after all
assessors have committed and a buffer period of 12 hours has elapsed.
The buffer ensures that there is time to challenge someone's commit,
even if they waited until moments before the end of the assessment
period to send it in. Should any assessor fail to reveal, their stake is
burned and they are eliminated from the assessment game. [^2] If the
number of assessors decreases below the necessary minimum, the rest of
the participants is being refunded and the assessment ends without a
score.

### Determining the Outcome {#sec:outcome}

In order for an assessment to result in a final score, one score must be in
consensus with enough other scores to form a 51% majority. Two scores are
considered to be in consensus if their difference is less than the
*consensus-distance* $\phi$. If such a score $s_{origin}$ exists, the final
score $s_{final}$ is computed as the average of all scores that are in consensus
with $s_{origin}$. 

Should there be two scores with majorities of equal sizes, the one that will
result in a lower final score wins. If there is no point of consensus, the
assessment is considered invalid and all stakes are burned. Otherwise the
assessors' payments will be computed as described in the following section.

Also, in case the result $s_{final}$ is a passing score, the assessee is
registered as new member of the concept with a weight $w_i = s_{final} *
N_{in}$. They get a weight in all parents and grandparent-concepts, reduced by
the respective connection weights until their weight is below a certain
threshold.

### Payout of Assessors {#sec:payout}

Payments to assessors consist of two parts, their returned stake and the assessee's
reward. Both are attributed differently, depending on whether or not the assessor
is inside out outside the majority cluster of winning assessors.

Therefore, an assessor $i$'s distance $dist_i$ from the final score $s_{final}$ is
measured against the consensus range $\phi$:

$$ dist_i = \frac{\lvert s_i - s_{final} \rvert}{\phi} $$

Outside assessors ($1<dist_i<2$) only get back a part of their stake, reduced
linearly in relation to their distance from the final cluster. Thus, an outside
assessor $j$'s payout is computed as:

  $$ payout_{out}_j = stake_j * \frac{(2 - dist_j)}{2} $$

Inside assessors ($dist_i <= 1) get back their entire stake, any stake that is
not returned to outside assessors (distributed equally among them) and a share
of the assessee's reward, proportional to their proximity to the final score:

$$ payout_{in}_i = stake_i + \frac{1}{N_{in}} * \sum_j^{N_{out}} (stake_j-payout_{out}_j) + r_a * (1 - dist_i) $$

,with $N_{out}$ and $N_{in}$ denoting the number of assessors outside
and inside the winning cluster and r_a being the reward of the assessee.

Thus, the best case scenario for an assessor is to be inside the winning
cluster, close to the final score, with a large minority outside of it.

Any payout that is not returned to the assessors will be burned (if it were
redistributed, assessors could collude to cover a range of scores and
redistribute amongst them without any loss).

<!-- Figure [1](/pages/appendix.html#payout) in the appendix summarizes the payout mechanism in a single graph.-->

[^2] This should be unlikely, as at that point assessors have nothing to lose but rewards to
gain.
