Status & Roadmap {#sec:status}
================

This section will describe the current state of the different components
the fathom-team is developing, how one can contribute and what we plan
to build in the future. It also describes our plans to instantiate and
initially support the network.

Implementation {#sec:implementation}
--------------

### Smart Contracts {#sec:sm}

The assessment process of the fathom protocol as described in this document is
fully implemented in solidity. The code, a testsuite and some supplementary
simulations are in our [gitlab repository](https://gitlab.com/fathom/assess).

### Front-end {#sec:frontend}

We are also working on various user-facing applications to interact with the
fathom protocol. Our first project was an arbitrary chat room application
aloowing the creation of social environments based on on-chain data, which was a
prototype for a platform for concept communities in fathom, as well as for a
space to conduct assessments. The code for this project is in [this
repository](https://gitlab.com/fathom/chat). While we still subscribe to the
idea of permissioned chatrooms, work is currently underway to refactor the work
into different, more abstract parts that can be reused in a modular fashion. The
best way to stay up-to-date is to follow in our [blog](http://fathom.network/blog) or in the [org-repo](https://gitlab.com/fathom/org)
in which we keep an updated roadmap.

How to Contribute {#sec:contribute}
-----------------

It's our philosophy to work out in the open as much as possible, so this
section will lay out the documents we are using to structure our work
and the process by which we invite volunteers to contribute.

All parts of our project are in our [GitLab
repository](https://gitlab.com/fathom/). The documents in the
[org-repo](https://gitlab.com/fathom/org) are a good place to start.

Also, we publish an approximately bi-weekly progress report on our
[blog](http://fathom.network/blog/) to keep you updated about the development
and about ways to engage with us and to help us prepare fathom for a successfull
launch.

We appreciate any interest in fathom and seek input from individuals
with all kinds of educational- or cryptoeconomic-related backgrounds. Feel
free to reach out to us at _contact@fathom.network_ or to create issues in our
repo.

Instantiating the network {#sec:instantiate}
-------------------------

In order to bootstrap the network, we will run a decentralized learning
bootcamp, aka the _fathom playground_. In brief, it will bring together a group
of motivated learners that will receive the initial batch of network-tokens and
use them to create credentials in order to coordinate their own learning. A
detailed explanation of the plaground will soon be published on our
[website](http://fathom.network/) and our [blog](http://fathom.network/blog).

Vision {#sec:vision}
======

We believe that a participatory protocol tied directly to the
communities practicing skills and defining ideas will diminish
the gap between credentials that can be credibly assessed and issued and
the wide variety of skills and abilities that people are capable of.

Tying economic incentives to this social process and ontology, such that
they are both visible to everybody and aligned amongst all those
participating, allows for fathom-credentials to be trustworthy and
transparent.

By distributing the work required to communities, allows the system to scale and
be accessible to anyone, no matter their previous records, achievements or
socio-economic circumstances.

We believe that through these traits fathom enables a world where people
are free to shape their own experiences, communicate them to others, and
organize to achive shared ambitions.

\pagebreak
