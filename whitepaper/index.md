---
abstract: "Fathom is a decentralized protocol to create meaningful credentials through the consensus of knowledge communities. Currently, most credentials rely on being backed by large institutions in order to be meaningful, which has led towards credentials that can be conveniently mass-customized and issued. This left most people in a situation where they can only reliably communicate a small part of their skill-set as dictated by the institutions. With fathom, anyone can create a credential for any kind of skill. In order to earn that credential, the fathom protocol defines an assessment-game, where the truthful evaluation of an applicant's skill is the schelling point. The economic incentivization of the fathom protocol makes users congregate around credentials that are well defined, in that they allow individuals to play the assessment game with a positive outcome. As such any community of any field can create their own credentials, use them in order to self-organize more effectively and its members can independently communicate their skills to the outside world."
title: "Fathom: A Protocol for Decentralized Assessment" 
---

\pagebreak
