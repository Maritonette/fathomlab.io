Security {#sec:security}
========================

The fathom protocol derives security from both it's technical implementation and
incentive structure. There are specific attack vectors that it mitigates against
as well as some areas for further research.

Incentive Structure {#sec:incentive}
-----------------------------

While traditional credentials are meaningful because they are backed by
reputable institutions, a fathom credential is meaningful because it is
the result of many individuals having undergone a financial risk in the
assessment game in order to create it. This section will lay out the
decisions fathom users face when participating in that game, as well as
the economic risks associated to them. Specifically, we show i) when
assessors are likely to participate in an assessment in the first place
and ii) why they can not collude with each other in order to shortcut
the work associated with a truthful assessment.

### Incentives for members to confirm or decline an assessment {#sec:acceptAss}

This case is especially relevant for creating new concepts - as those
will be initially empty and rely on members of its parent to participate
in as assessors. A member of a concept who has received an offer to be
an assessors will consider whether...

-   They feel competent enough in their abilities to come to the same
    conclusion as a group of other, randomly selected assessors that are confident in *their*
    abilities.

-   The concept in question is well enough defined so that assessors
    with similar impressions of the assessee's skill will be able to
    translate these into similar scores.
    
-   Whether its connection strength is appropriately chosen. A passing score in
    the concept in question will result in the assessee being added to the
    assessors concept, and therefore affect the assessor's chances in future
    assessment games, as well as the quality of the community. If the connection
    strength would be high but the concept comparatively easy to earn, the
    assessor's community could be flooded and their chances of being called as
    an assessor reduced.

### Incentives for assessors to grade truthfully {#sec:notCollude}

As truthfully assessing someone requires effort and the assessors payout is
pegged to their alignment with each other, there is a motivation for them to
collude, e.g. by agreeing ahead of time which score to commit. The creation of
an adverserial environment between assessors is thus vital for the protocol to
function as intended.\ Therefore, several mechanisms are put in place: First,
assessors are paid out more if there are dissenting assessors
( [figure 2](#fig:payoutDiss){reference-type="ref" reference="fig:payoutDiss"}).
Consequently, any assessor taking part in a collusion of X assessors, must be
afraid that they will be double crossed by a subgroup of more than $X/2$
assessors. These are motivated to do so because they would be rewarded with part
of the crossed assessors stake. Moreover, it is not possible for assessors to
credibly prove to another assessor that they have actually committed to a
collusion and logged in a previously agreed-upon score. In order to do so, the
proving party would have to reveal their score and salt to the other assessors.
Yet with this information, the other assessors could
simply [steal](#sec:stealReveal){reference-type="ref"
reference="sec:stealReveal"} the assessors stake, which would eliminate the
former assessor from the assessment and directly transfer the half of the
revealed assessor's stake to the revealer.


Attack Vectors {#sec:attackVectors}
---------------------

This section will outline some of the general classes of attacks against the
protocol and a subjective view of their complexity, severity and to what degree
they are considered to be mitigated.

### Sybil Attacks {#sec:sybil}

In a sybil attack, the attacker creates many false identities and then
uses them to subvert the system, e.g. by controlling most of the
identities in a concept, giving him control over who will be accepted
and the ability to create assessments for himself in order to steal the
stakes of other assessors.

To set up such an attack the attacker would, instead of being assessed
by many assessors in one assessment, create multiple assessments with
fewer assessors. This would be the same amount of work but result in
four identities in the concept. Repeating the procedure, the attacker
could count on some of his identities being called as assessors in which
case the subsequent repetitions would become cheaper and less
time-consuming until they have the majority in the concept or are called
multiple times as assessor such that they can set up a 51% attack on
individual assessments. In such a scenario, the attacker could control
the outcome of the assessment and steal the stake of the other
assessors.

**Severity of attack**: While a sybil attack does cost a fair amount of
money to set up, the potential benefits are big enough to incentivize a
try. As a compromised concept can potentially 'poison' its parent
concepts as well and thus potentially effect the entire tree, we
consider it to very severe.

**Complexity**: While a sybil attack is fairly complex, it can be
effectuated by a single attacker, which is why it would be careless to
assume that the degree of complexity will be a deterrent factor.

**Degree of Protection**: One possible mitigation that is not yet part
of the protocol, will be to split the certificate and the right to be an
assessor in two separate assessments. While this does not address the
fundamental issue, it makes it easier for the sybil-protection measures
to be integrated into the assessment process. For example, the
assessment to become an assessor could ask the to-be-assessors for some
piece of their own work or something that is new and can not be readily
found on the internet as would be the case with the mere knowledge or
skill required in the concept.

### Simple Trolling {#sec:trolling}

A troll, for arbitrary reasons, might try to poison the fathom network
by creating a bunch of bogus assessments or concepts or by behaving
irrationaly while being an assessor. In all cases, such behavior is
expensive and ineffective, as his stakes are burned (when not following
through with an assessment) or redistributed to others (when logging in
bogus scores).

Bogus concepts will simply incur costs on the troll and be filtered out
by assessors (see [incentives](#sec:acceptAss){reference-type="ref"
reference="sec:acceptAss"}). Creating bogus assessments as assessee will
be even more costly (transaction costs and the fees for assessors). The
worst effect a troll can have is to become an assessors and to
prematurely end the assessment, if as a consequence of their behavior,
its size is reduced below the minimum of five. In that case all other
participants will be refunded, though.

**Complexity**: Behaving irrationaly is simple and so is attacking the
system this way.

**Severity**: With no financial costs to other participants these kinds
of attacks are not considered severe. An exception might be the creation
of concepts, which if done by a well-resourced attacker, amounts to
spamming the system.

**Degree of protection**: We consider simple trolling to be sufficiently
discouraged because of the associated costs. If such behavior would be
escalated into a spam-attack of greater proportions, the degree of
protection will depend on the users or the fathom frontends ability to
filter concepts and assessments by meaningful criteria.

### P + epsilon attack {#sec:blackmail}

In a P + epsilon attack, the attacker circumvents the incentivization
by creating a mechanism that others can trust in because it gives them a
credible guarantee about the attacker's behavior. While this would have
been difficult in a pre-blockchain era, smart contracts are nearly
ideally suited to implement such mechanisms.

The attack works like this: In a schelling-point game, the assessors are
being paid out the same amount $P$. regardless of the result (option A,
B, C or any other\...). The attacker, let's say wanting to push for a
certain option A, will credibly guarantee anyone voting for A that he
will be paid P+$\epsilon$, if they vote A and the majority doesn't.
Assuming a system that is not dominated by altruistic actors, voting A
is now the game-theoretically best option (guaranteed maximal payout).
Therefore, the majority will vote A and the attacker will have taken
over the mechanism - at zero cost.

Although there exist some protection mechanisms that can increase the
attackers risk (size of the needed bribe) and some counter-coordination
mechanisms that come close to defeating such an attack, there is
currently no guaranteed countermeasure.

**Complexity**: As the crucial element of this attack is the mechanism
by which the attacker commits to his intention to paying out in case the
bribed voter is not in the majority, the complexity is proportional to
the difficulty of construing such a mechanism. In the case of fathom,
the difficulty to reconstruct the relevant information (did an assessor
really vote for the desired option A?). Currently, this is rather
simple, so setting up this attack would not be very complex.

**Severity**: As this attack can disrupt the system at potentially
zero-cost, we consider it to be very severe.

**Degree of Protection**: As of right now, the protocol is not protected
against such measurs. Future versions of it could implement some more
complicated schemes in order to keep the scores of individual assessors
secret and make it harder to retrieve the individual assessors' scores.

[^1]: Although it's possible to repeat an assessment, only the result of
    the most recent assessment will be taken into account for the
    weight.

[^2]: This should be unlikely, as at that point assessors have nothing
    to lose but rewards to gain.

\pagebreak
