\pagebreak
Introduction
============

Society functions on knowing what people can do. Everybody needs to be able
to communicate their skills to others in order to coordinate. This used to
be a social process within a local community, but society has grown and
largely outsourced that function to institutions. Today, people need to
communicate a greater diversity of skills and experiences than ever before,
over long timescales and across geographic, cultural and linguistic
barriers. It is our belief that institutions cannot provide that service
and will be unable to provide it in the future.

We seek to provide an alternative to institutional credentials by
specifying a social protocal with economic incentives to allow knowledge
communities to define their own standards and individuals to be assessed
resulting in meaningful, verifiable and durable credentials

Problem Statement {#sec:problem}
-----------------

The coupling of learning and assessment in current institutional models is
unscalable and creates a set of perverse incentives for both educators and
students. The bureaucracy of centralized institutions makes them resource
intensive and slow to adapt to changes. As a consequence they are only able
to offer a small set of experiences, defaulting to those that can be
mass-produced.

Because communicating one's experiences is so essential in today's society,
it is in an individual's best interest to actively mold their experiences
towards what they can communicate, instead of towards what they can aspire
to. Therefore, relying on instituions to be the arbiter of people's
abilities has a chilling effect on societal progress.

Introducing Fathom {#sec:introducing}
------------------

Fathom is a protocol to create and assess meaningful credentials through
the consensus of knowledge communities.

It allows anyone to create a credential and anyone to be assessed in it.
The core process involves a jury of randomly assembled assessors with
relevant experience, as previously proven by the protocol, playing an
'assessment-game' in which they are economically incentivized such that
an accurate assessment is the schelling point.

The protocol makes no assumptions about what is being assessed, instead
it allows communities to form their own definitions and rules, to be
then carried out collectively.

Implemented on a public blockchain, it will be possible to distribute
the work necessary for assessments to scale far beyond what institutions
are capable of.

Furthermore, blockchains can enable a truly inclusive, accessible and
extensible credential-ecosystem, which is censor-resistant, durable and
leaves individuals in full control of their identities: Able to learn
skills and accumulate experiences towards their unique aims as well as
to shape and strenghten the network by participating as an assessor.

Outline {#sec:outline}
-------

The purpose of this document is two-fold: to provide a formal
specification of the fathom-protocol, and to describe the infrastructure
necessary to deploy it on a large scale. The second part will also lay
out the launching process that we think will be favorable for a
widespread adoption and for the overall intregrity of the system. While
the core protocol is unlikely to change much, the infrastructure around
it is still being developed and as such this document is subject to
change. We believe in transparency and working out in the open and
therefore invite the reader to engage with us: By contributing ideas or
comments, or by actively taking part in the development. Together, we
seek to enable a wide variety of real-world applications based on this
new layer of trust for digital social relationships.

\pagebreak
