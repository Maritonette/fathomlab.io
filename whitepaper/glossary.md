Glossary
=========

### Concept
Represents the shared knowledge, skillset, or other attribute that the users who have attained the concept have in commom. Examples could range from Calculus proficiency to English fluency to marathon completion.

### Parents
A parent of a concept is one more general than it. For example Calculus is a child of Math and a parent of Differentiation. When an individual is successfully earn a weight in a concept they also own a lower weight in it's parents.

### Assessor
A member of an Assessment who has previously acquired the Concept the Assessee(s) are trying to attain.

### Assessee
The subject and iniator of an assessment, who wants to become a member of a concept. 

### Tokens
An ERC20 token that is used to pay for assessments, and is earned by assessors. It is created upon succesful assessments where more is paid out than what is paid by the assessee.

### Assessment
The process by which an Assessee/Assessees attain Concepts and Assessors attain tokens. The Assessee/Assessees initiate an Assessment by paying a number of Tokens and inputting a Concept. A number of Assessors, who have previously been favourably assessed on the Concept, are put into communication with the Assessee(s). Each Assessor then scores the Assessee(s) from -100 to 100 and the scores are averaged. Assessors who score near the average are rewarded with a greater Token payout. If the score is positive the Assessee/Assessees attain the Concept.

### Weight
Upon a successful assessment an individual earns a weight in a concept. This is calculated as a function of their score, the number of assessors in the largest cluster, and the time the assessment was taken.
