# Building
1. Install [pandoc](http://pandoc.org/installing.html) and make sure its on your path
2. clone this repo somewhere `git clone https://gitlab.com/fathom/fathom.gitlab.io.git`
3. run the build.sh script `cd fathom.gitlab.io; ./build.sh`
4. serve the public directory with something like [simplehttpserver](https://www.npmjs.com/package/simplehttpserver) `simplehttpserver public`