---
title: about
---

Fathom is a Ethereum based protocol. It is principally being developed by [Jared Pereira](http://otlw.co), Julius Faber and Michiya Hibino at [ConsenSys](https://consensys.net/).
